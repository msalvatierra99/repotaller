var ClientesObtenidos;

function getClientes(){
var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
var request= new XMLHttpRequest();

request.onreadystatechange = function(){

  if(this.readyState==4 && this.status==200){
      //console.log(request.responseText);
      ClientesObtenidos=request.responseText;
      procesarClientes();
     }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarClientes(){
var JSONClientes = JSON.parse(ClientesObtenidos);
var divTabla = document.getElementById("divClientes");
var tabla = document.createElement("table");
var tbody = document.createElement("body");

tabla.classList.add("table");
tabla.classList.add("table-striped");

//alert(JSONProductos.value[0].ProductName);
for (var i = 0; i < JSONClientes.value.length; i++) {
  //  console.log(JSONProductos.value[i].ProductName);
  var nuevaFila= document.createElement("tr");
  var columnaNOmbre= document.createElement("td");
  columnaNOmbre.innerText = JSONClientes.value[i].ContactName;
  var columnaCiudad= document.createElement("td");
  columnaCiudad.innerText = JSONClientes.value[i].City;
  var columnaBandera= document.createElement("td");
  var imgBandera = document.createElement("img");
  imgBandera.classList.add("flag");
  if(JSONClientes.value[i].Country=="UK"){
    imgBandera.src="https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
  }
  else {
    imgBandera.src="https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png"
  }
  columnaBandera.appendChild(imgBandera);

  nuevaFila.appendChild(columnaNOmbre);
  nuevaFila.appendChild(columnaCiudad);
  nuevaFila.appendChild(columnaBandera);

  tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
